/*
 * This file is part of DHTT (DbgHelp Test Tool)
 *
 * Wine-like test framework, so that tests could be integrated into Wine.
 *
 * Copyright (C) 2021 Eric Pouech
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

static unsigned ok_count;
static unsigned ok_count_failed;
static unsigned ok_count_todo;
static const char* ok_file;
static unsigned ok_line;
static char* ok_context[4];
static unsigned ok_context_index;
static int ok_intodo;
static int ok_todocond;
static unsigned ok_running_wine;

static inline void __ok(int c, const char* msg, ...)
{
    unsigned intodo = ok_intodo && ok_todocond;
    if (!c || intodo)
    {
        va_list args;
        if (intodo)
        {
            if (c) printf("Test succeeded inside todo:\n\t");
            else printf("todo: ");
        }
        if (ok_context_index)
        {
            unsigned i;
            for (i = 0; i < ok_context_index; ++i)
                printf("%s:", ok_context[i]);
            printf(" ");
        }
        printf("%s:%u:", ok_file, ok_line);
        va_start(args, msg);
        vprintf(msg, args);
        va_end(args);
        if (!c && !intodo) ok_count_failed++; else ok_count_todo++;
    }
    ok_count++;
}
#define ok    ok_file=__FILE__,ok_line=__LINE__,__ok

static inline const char* debugstr_a(const char* s) {return s;}
static inline char* wine_dbgstr_longlong(ULONG64 u)
{
    static char buf[2048];
    static char* ptr;

    ptr = (ptr) ? ptr + strlen(ptr) + 1 : buf;

    if (ptr + 32 >= buf + sizeof(buf)) ptr = buf;
    sprintf(ptr, "0x%lx%08lx", (ULONG)(u >> 32), (ULONG)u);
    return ptr;
}

static inline void winetest_push_context(const char* msg, ...)
{
    va_list args;

    assert(ok_context_index < sizeof(ok_context) / sizeof(ok_context[0]));
    ok_context[ok_context_index] = malloc(1024);

    va_start(args, msg);
    vsnprintf(ok_context[ok_context_index], 1024, msg, args);
    va_end(args);
    ok_context_index++;
}

static inline void winetest_pop_context(void)
{
    assert(ok_context_index);
    free(ok_context[--ok_context_index]);
}

#define todo_for(x) for (ok_todocond = (x),ok_intodo++;ok_intodo;ok_intodo--)
#define todo_wine todo_for(ok_running_wine)
#define todo_wine_if(x) todo_for(ok_running_wine && (x))

static inline void winetest_init(void)
{
    HMODULE module = GetModuleHandleA("ntdll.dll");
    if (module && GetProcAddress(module, "wine_server_call") != NULL)
        ok_running_wine = 1;
}

static inline void winetest_report(void)
{
    printf("Tests run %u", ok_count);
    if (ok_count_failed) printf(", failed %u", ok_count_failed);
    if (ok_count_todo) printf(", todo %u", ok_count_todo);
    printf("\n");
}
