-include Makefile.config

CC64=x86_64-w64-mingw32-gcc
CC32=i686-w64-mingw32-gcc

CFLAGS=-I$(WINE_SRC)/include -I$(WINE_SRC)/include/msvcrt \
-D__WINESRC__ -D_MSVCR_VER=0 -DWINE_CROSS_PE \
-Wall -fno-strict-aliasing -Wdeclaration-after-statement -Wempty-body -Wignored-qualifiers -Winit-self \
-Wno-packed-not-aligned -Wshift-overflow=2 -Wstrict-prototypes -Wtype-limits \
-Wunused-but-set-parameter -Wvla -Wwrite-strings -Wpointer-arith -Wlogical-op -Wabsolute-value
CFLAGS64=$(CFLAGS) -I$(WINE64)/include -Wno-format -Wformat-overflow -Wnonnull -mcx16
CFLAGS32=$(CFLAGS) -I$(WINE32)/include -fno-omit-frame-pointer

# we manage three different kinds of objects:
# - the various wine systems: wineXX
# - the various debuggees debuggee-{target}-{compiler}-{dbgformat}-{options}.dll
# - the various tests programs: dhtt*.exe

.PHONY: dhtt test test32 test64 teststrictwow clean distclean debuggee

.NOTPARALLEL:

all: dhtt debuggee

dhtt: dhtt64.exe dhtt32.exe

dhtt64.exe: main.c dbginfo.c test.h dhtt.h
	$(CC64) -c main.c dbginfo.c $(CFLAGS64) -g -gdwarf-2 -O2
	$(WINE64)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE64) \
		-b x86_64-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin -mconsole \
		main.o dbginfo.o \
		$(WINE64)/dlls/msvcrt/libmsvcrt.cross.a $(WINE64)/dlls/dbghelp/libdbghelp.cross.a

dhtt32.exe: main.c dbginfo.c test.h dhtt.h
	$(CC32) -c main.c dbginfo.c $(CFLAGS32) -g -gdwarf-2 -O2
	$(WINE32)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE32) \
		-b i686-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin -mconsole \
		main.o dbginfo.o \
		$(WINE32)/dlls/msvcrt/libmsvcrt.cross.a $(WINE32)/dlls/dbghelp/libdbghelp.cross.a

SRC_DEBUGGEES=debuggee.c
OBJ_DEBUGGEES=debuggee.o

debuggee: debuggee-pe64-mingw-dwarf4-O1.dll debuggee-pe64-mingw-dwarf4-O2.dll \
	  debuggee-pe64-mingw-dwarf2-O1.dll debuggee-pe64-mingw-dwarf2-O2.dll \
	  debuggee-pe32-mingw-dwarf4-O1.dll debuggee-pe32-mingw-dwarf4-O2.dll \
	  debuggee-pe32-mingw-dwarf2-O1.dll debuggee-pe32-mingw-dwarf2-O2.dll \

debuggee-pe64-mingw-dwarf4-O1.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC64) -c $$dbg $(CFLAGS64) -g -gdwarf-4 -gstrict-dwarf -O1; \
	done
	$(WINE64)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE64) \
		-b x86_64-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE64)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe64-mingw-dwarf4-O2.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC64) -c $$dbg $(CFLAGS64) -g -gdwarf-4 -gstrict-dwarf -O2; \
	done
	$(WINE64)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE64) \
		-b x86_64-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE64)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe64-mingw-dwarf2-O1.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC64) -c $$dbg $(CFLAGS64) -g -gdwarf-2 -gstrict-dwarf -O1; \
	done
	$(WINE64)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE64) \
		-b x86_64-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE64)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe64-mingw-dwarf2-O2.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC64) -c $$dbg $(CFLAGS64) -g -gdwarf-2 -gstrict-dwarf -O2; \
	done
	$(WINE64)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE64) \
		-b x86_64-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE64)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe32-mingw-dwarf4-O1.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC32) -c $$dbg $(CFLAGS32) -g -gdwarf-4 -gstrict-dwarf -O1; \
	done
	$(WINE32)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE32) \
		-b i686-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE32)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe32-mingw-dwarf4-O2.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC32) -c $$dbg $(CFLAGS32) -g -gdwarf-4 -gstrict-dwarf -O2; \
	done
	$(WINE32)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE32) \
		-b i686-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE32)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe32-mingw-dwarf2-O1.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC32) -c $$dbg $(CFLAGS32) -g -gdwarf-2 -gstrict-dwarf -O1; \
	done
	$(WINE32)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE32) \
		-b i686-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE32)/dlls/msvcrt/libmsvcrt.cross.a

debuggee-pe32-mingw-dwarf2-O2.dll: $(SRC_DEBUGGEES)
	for dbg in $(SRC_DEBUGGEES); do \
		$(CC32) -c $$dbg $(CFLAGS32) -g -gdwarf-2 -gstrict-dwarf -O2; \
	done
	$(WINE32)/tools/winegcc/winegcc -o $@ --wine-objdir $(WINE32) \
		-b i686-w64-mingw32 --lib-suffix=.cross.a -mno-cygwin \
		-shared debuggee.spec $(OBJ_DEBUGGEES) \
		$(WINE32)/dlls/msvcrt/libmsvcrt.cross.a

test: test32 test64

test64: dhtt debuggee
	@./run.bash "$(WINE64)"    dhtt64.exe dhtt64.exe -- debuggee-pe64-*.dll
	@./run.bash "$(WINE64)"    dhtt64.exe --         -- debuggee-pe64-*.dll

# FIXME	./run.bash "$(WINEWOW64)" dhtt32.exe dhtt64.exe -- debuggee-pe64-*.dll

test32: dhtt debuggee
	@./run.bash "$(WINE32)"    dhtt32.exe dhtt32.exe -- debuggee-pe32-*.dll
	@./run.bash "$(WINE32)"    dhtt32.exe --         -- debuggee-pe32-*.dll
	@./run.bash "$(WINEWOW64)" dhtt64.exe dhtt32.exe -- debuggee-pe32-*.dll

# we assume that there won't be much difference wrt. equivalent tests on
# WINEWOW64 resp. WINE64
# keep the targets if someone really wants to be reassured, but don't include in test target
teststrictwow: dhtt debuggee
	@./run.bash "$(WINEWOW64)" dhtt64.exe --         -- debuggee-pe64-*.dll
	@./run.bash "$(WINEWOW64)" dhtt64.exe dhtt64.exe -- debuggee-pe64-*.dll
	@./run.bash "$(WINEWOW64)" dhtt32.exe --         -- debuggee-pe32-*.dll
	@./run.bash "$(WINEWOW64)" dhtt32.exe dhtt32.exe -- debuggee-pe32-*.dll

# don't erase dll/pdb generated on windows
clean:
	rm -f *.exe *.o *.obj *.ilk *.exp *.lib debuggee-*-mingw-*.dll

distclean: clean
	rm -f *.orig *.rej *~ *.dll *.pdb
