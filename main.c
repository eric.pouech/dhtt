/*
 * This file is part of DHTT (DbgHelp Test Tool)
 *
 * Entry point for managing command line and driving internals.
 *
 * Copyright (C) 2021 Eric Pouech
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "dhtt.h"
#include <dbghelp.h>

static void usage(const char* error, const char* pmt)
{
    fprintf(stderr, "Error %s", error);
    if (pmt)
        fprintf(stderr, ": %s", pmt);
    fprintf(stderr, "\n");
    exit(1);
}

enum modes {INTERNAL, EXTERNAL, JUSTLOAD};

static void wait_thread(const struct debuggee* dbg)
{
    DWORD count;
    /* wait for the thread to suspend itself */
    do
    {
        Sleep(50);
        count = SuspendThread(dbg->thread);
        ResumeThread(dbg->thread);
    }
    while (!count);
}

static BOOL init_internal_process(struct debuggee* dbg)
{
    HMODULE module = LoadLibraryA(dbg->dll_name);
    LPTHREAD_START_ROUTINE ptr;

    if (!module)
    {
        fprintf(stderr, "Couldn't load library %s\n", dbg->dll_name);
        return FALSE;
    }
    ptr = (LPTHREAD_START_ROUTINE)GetProcAddress(module, "stack_walk_thread");
    if (ptr == NULL)
    {
        fprintf(stderr, "Couldn't find stack_walk_thread\n");
        return FALSE;
    }
    dbg->external_process = FALSE;
    dbg->process = GetCurrentProcess();
    dbg->thread = CreateThread(NULL, 0, ptr, NULL, 0, NULL);
    if (dbg->thread == NULL)
    {
        fprintf(stderr, "Couldn't start thread %p\n", ptr);
        return FALSE;
    }
    dbg->machine = IMAGE_FILE_MACHINE_UNKNOWN;
    dbg->base = dbg->size = 0;
    wait_thread(dbg);
    return TRUE;
}

static BOOL init_external_process(struct debuggee* dbg, const char* subpcs)
{
    STARTUPINFOA siA;
    PROCESS_INFORMATION pi;
    char cmdline[MAX_PATH];

    memset(&siA, 0, sizeof(siA));
    siA.cb = sizeof(siA);
    memset(&pi, 0, sizeof(pi));

    snprintf(cmdline, sizeof(cmdline), "%s --dont-test --dll %s", subpcs, dbg->dll_name);

    if (!CreateProcessA(NULL, cmdline, NULL, NULL, FALSE,
                        NORMAL_PRIORITY_CLASS|DETACHED_PROCESS, NULL, NULL, &siA, &pi))
    {
        fprintf(stderr, "Couldn't start subprocess %s\n", subpcs);
        return FALSE;
    }
    dbg->external_process = TRUE;
    dbg->process = pi.hProcess;
    dbg->thread = pi.hThread;
    dbg->machine = IMAGE_FILE_MACHINE_UNKNOWN;
    dbg->base = dbg->size = 0;
    wait_thread(dbg);

    return TRUE;
}

static void terminate_process(const struct debuggee* dbg)
{
    ResumeThread(dbg->thread);
}

/* wine's dbghelp is currently broken:
 * when loading a module with only dwarf4 debug info, it reports that
 * debug info is present (as it has found the relevant ELF/PE sections)
 * but fails to handle the case where all CUs are in the wrong format.
 * So we have to detect this on our own (until wine's dbghelp is fixed)
 */
static BOOL has_debug_info(const IMAGEHLP_MODULE* im)
{
    return im->SymType == SymPdb || im->NumSyms > 4;
}

static BOOL CALLBACK find_debuggee_module_cb(PCSTR name, DWORD64 base, PVOID in_user)
{
    struct debuggee* dbg = in_user;
    IMAGEHLP_MODULE im;

    memset(&im, 0, sizeof(im));
    im.SizeOfStruct = sizeof(im);
    if (SymGetModuleInfo(dbg->process, base, &im) && has_debug_info(&im))
    {
        /* lookup external exec path as module name */
        const char* ff;
        const char* fb;
        const char* ext;

        if ((ff = strrchr(dbg->dll_name, '/')) == NULL) ff = dbg->dll_name; else ++ff;
        if ((fb = strrchr(ff, '\\')) == NULL) fb = ff; else ++fb;

        ext = strchr(fb, '.');
        if ((!ext || !_strcmpi(ext, ".dll")) && !_strnicmp(fb, im.ModuleName, ext ? ext - fb : strlen(fb)))
        {
            dbg->base = im.BaseOfImage;
            dbg->size = im.ImageSize;
            return FALSE;
        }
    }

    return TRUE;
}

static BOOL start_debug_info(struct debuggee* dbg)
{
    USHORT pcs, native;

    SymSetOptions(SymGetOptions() | SYMOPT_INCLUDE_32BIT_MODULES);

    // FIXME until we fix IMAGEHLP_MODULE64 wrt MachineType field
    if (!IsWow64Process2(dbg->process, &pcs, &native))
    {
        fprintf(stderr, "Cannot get process information\n");
        return FALSE;
    }
    dbg->machine = pcs == IMAGE_FILE_MACHINE_UNKNOWN ? native : pcs;
    if (!SymInitialize(dbg->process, NULL, TRUE))
    {
        fprintf(stderr, "Cannot init dbghelp\n");
        return FALSE;
    }

    if (!SymEnumerateModules64(dbg->process, find_debuggee_module_cb, dbg) || dbg->base == 0)
    {
        fprintf(stderr, "Couldn't init module %s\n", dbg->dll_name);
        SymCleanup(dbg->process);
        return FALSE;
    }

    return TRUE;
}

static void stop_debug_info(struct debuggee* dbg)
{
    SymCleanup(dbg->process);
}

int main(int argc, char** argv)
{
    int i;
    struct debuggee dbg;
    enum modes mode = INTERNAL;
    const char* subpcs = NULL;

    dbg.dll_name = NULL;
    for (i = 1; i < argc; ++i)
    {
        if (!strcmp(argv[i], "--subprocess") && ++i < argc) {mode = EXTERNAL; subpcs = argv[i];}
        else if (!strcmp(argv[i], "--dont-test")) mode = JUSTLOAD;
        else if (!strcmp(argv[i], "--dll") && ++i < argc) dbg.dll_name = argv[i];
        else
            usage("unknown option", argv[i]);
    }
    if (!dbg.dll_name) usage("Must specify a DLL", NULL);
    switch (mode)
    {
    case INTERNAL:
        if (init_internal_process(&dbg))
        {
            if (start_debug_info(&dbg))
            {
                do_tests(&dbg);
                stop_debug_info(&dbg);
            }
            terminate_process(&dbg);
        }
        break;
    case EXTERNAL:
        if (init_external_process(&dbg, subpcs))
        {
            if (start_debug_info(&dbg))
            {
                do_tests(&dbg);
                stop_debug_info(&dbg);
            }
            terminate_process(&dbg);
        }
        break;
    case JUSTLOAD:
        {
            HMODULE module = LoadLibraryA(dbg.dll_name);
            LPTHREAD_START_ROUTINE ptr;
            if (module && (ptr = (LPTHREAD_START_ROUTINE)GetProcAddress(module, "stack_walk_thread")) != NULL)
                (ptr)(NULL);
        }
        break;
    }
    return 0;
}
