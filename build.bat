@if /I "%1"=="test" goto :test
@if /I "%1"=="clean" goto :clean
@if /I "%1"=="__compile32" goto :__compile32
@if /I "%1"=="__compile64" goto :__compile64

@SET VSBUILD=C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools
@SET "VCVARS=C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat"

@SET "CFLAGS=/Zi /W4 -I "%VSBUILD%\DIA SDK\include""

@cmd.exe /c "%0 __compile64"
@cmd.exe /c "%0 __compile32"

@goto :EOF

:__compile64
call "%VCVARS%" x64
CL.EXE %CFLAGS% /Od main.c dbginfo.c /Fedhtt64-msvc.exe /MD /link /SUBSYSTEM:CONSOLE MSVCRT.lib Kernel32.lib DbgHelp.lib
CL.EXE %CFLAGS% /O1 debuggee.c /c
LINK.EXE debuggee.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-O1.dll /SUBSYSTEM:CONSOLE /EXPORT:stack_walk_thread Kernel32.lib
CL.EXE %CFLAGS% /O2 debuggee.c /c
LINK.EXE debuggee.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-O2.dll /SUBSYSTEM:CONSOLE /EXPORT:stack_walk_thread Kernel32.lib

@goto :EOF

:__compile32
call "%VCVARS%" x64_x86
CL.EXE %CFLAGS% /Od main.c dbginfo.c /Fedhtt32-msvc.exe /MD /link /SUBSYSTEM:CONSOLE MSVCRT.lib Kernel32.lib DbgHelp.lib
CL.EXE %CFLAGS% /O1 debuggee.c /c
LINK.EXE debuggee.obj /DLL /DEBUG /OUT:debuggee-pe32-msvc-pdb-O1.dll /SUBSYSTEM:CONSOLE /EXPORT:stack_walk_thread Kernel32.lib
CL.EXE %CFLAGS% /O2 debuggee.c /c
LINK.EXE debuggee.obj /DLL /DEBUG /OUT:debuggee-pe32-msvc-pdb-O2.dll /SUBSYSTEM:CONSOLE /EXPORT:stack_walk_thread Kernel32.lib

@goto :EOF

:clean
erase debuggee-pe64-msvc-pdb-O1.dll
erase debuggee-pe64-msvc-pdb-O2.dll
erase debuggee-pe32-msvc-pdb-O1.dll
erase debuggee-pe32-msvc-pdb-O2.dll
erase *.exp *.lib *.pdb *.ilk *.obj dhtt*-msvc.exe

@goto :EOF

:test
dhtt64-msvc.exe --subprocess dhtt64-msvc.exe --dll debuggee-pe64-msvc-pdb-O1.dll
dhtt64-msvc.exe --dll debuggee-pe64-msvc-pdb-O1.dll
dhtt64-msvc.exe --subprocess dhtt64-msvc.exe --dll debuggee-pe64-msvc-pdb-O2.dll
dhtt64-msvc.exe --dll debuggee-pe64-msvc-pdb-O2.dll
dhtt32-msvc.exe --subprocess dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O1.dll
dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O1.dll
dhtt32-msvc.exe --subprocess dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O2.dll
dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O2.dll
dhtt64-msvc.exe --subprocess dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O1.dll
dhtt64-msvc.exe --subprocess dhtt32-msvc.exe --dll debuggee-pe32-msvc-pdb-O2.dll

@goto :EOF

:EOF
