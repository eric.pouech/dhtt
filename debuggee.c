/*
 * This file is part of DHTT (DbgHelp Test Tool)
 *
 * Entry point for managing command line and driving internals.
 *
 * Copyright (C) 2021 Eric Pouech
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <windows.h>

#if defined(__GNUC__)
#define NOINLINE __attribute__((noinline))
#elif defined(_MSC_VER)
#define NOINLINE __declspec(noinline)
#else
#define NOINLINE
#endif

NOINLINE DWORD CALLBACK stack_walk_thread(void *arg)
{
    return SuspendThread(GetCurrentThread());
}

struct foobar
{
    char ch;
    char* array[12];
    unsigned bf1 : 3, bf2 : 7, bf3 : 2;
    unsigned u;
};

static struct foobar myfoobar;

enum myenum
{
    FIRST_VALUE,
    SECOND_VALUE,
    THIRD_VALUE = 57,
    FOURTH_VALUE,
};

static enum myenum enum_value = FIRST_VALUE;

NOINLINE unsigned function_AA(struct foobar* fb, DWORD64 base)
{
    DWORD other = (DWORD)base;
label:
    fb->u += other + enum_value;
    if (fb->ch != 0) goto label;
    return fb->bf1 == 0;
}

BOOL WINAPI DllMain(HINSTANCE instance_new, DWORD reason, LPVOID reserved)
{
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
        function_AA(&myfoobar, (DWORD_PTR)reserved);
        break;
    }
    return TRUE;
}
