/*
 * This file is part of DHTT (DbgHelp Test Tool)
 *
 * Basic tests for debug information gotten from dbghelp.
 *
 * Copyright (C) 2021 Eric Pouech
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "test.h"
#include "dhtt.h"
#include <cvconst.h>
#include <dbghelp.h>
#include <oaidl.h>

static BOOL endswithW(const WCHAR* in, const WCHAR* with)
{
    SIZE_T inlen = lstrlenW(in);
    SIZE_T withlen = lstrlenW(with);

    if (inlen < withlen) return FALSE;
    return _wcsnicmp(in + inlen - withlen, with, withlen) == 0;
}

static BOOL isfile(const char* str, const char* ext, WCHAR* name)
{
    DWORD sz1 = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0) - 1;
    DWORD sz2 = MultiByteToWideChar(CP_ACP, 0, ext, -1, NULL, 0);
    WCHAR* nameW;
    BOOL ret = FALSE;

    if ((nameW = HeapAlloc(GetProcessHeap(), 0, (sz1 + sz2) * sizeof(WCHAR))) != NULL)
    {
        MultiByteToWideChar(CP_ACP, 0, str, -1, nameW, sz1);
        MultiByteToWideChar(CP_ACP, 0, ext, -1, nameW + sz1, sz2);
        ret = endswithW(name, nameW);
        HeapFree(GetProcessHeap(), 0, nameW);
    }
    return ret;
}

static BOOL ismodulename(const char* dllname, const WCHAR* modname)
{
    size_t len = strlen(dllname);
    const char* first;
    const char* last;
    DWORD sz;
    WCHAR* wstr;
    int ret;

    if (len < 4) return FALSE;
    last = dllname + len - 4;
    if (strcmp(last, ".dll")) return FALSE;
    first = dllname; // FIXME should handle path
    sz = MultiByteToWideChar(CP_ACP, 0, first, (DWORD)(last - first), NULL, 0);
    wstr = HeapAlloc(GetProcessHeap(), 0, sz * sizeof(WCHAR));
    if (!wstr) return FALSE;
    MultiByteToWideChar(CP_ACP, 0, first, (DWORD)(last - first), wstr, sz);
    ret = wcsnicmp(wstr, modname, sz);
    HeapFree(GetProcessHeap(), 0, wstr);
    return ret == 0;
}

/* When looking at a typedef (like DWORD), compilers & debug formats differ:
 * - some use the type of the typedef
 * - some others use the target of the typedef (especially, when it's a well defined
 *   type for the debug format)
 * Cope with this by removing all typedef:s from the debug info to get to the
 * target type.
 */
static DWORD untypedef(const struct debuggee* dbg, DWORD type)
{
    DWORD tag;
    BOOL ret = TRUE;

    while (ret &&
           SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag) &&
           tag == SymTagTypedef)
    {
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    }
    return type;
}

static TI_FINDCHILDREN_PARAMS* retrieve_children(const struct debuggee* dbg, DWORD id)
{
    DWORD count;
    BOOL ret;

    ret = SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_CHILDRENCOUNT, &count);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());

    if (ret)
    {
        TI_FINDCHILDREN_PARAMS* tfp;

        tfp = HeapAlloc(GetProcessHeap(), 0, sizeof(*tfp) + count * sizeof(tfp->ChildId[0]));
        if (tfp)
        {
            tfp->Count = count;
            tfp->Start = 0;
            ret = SymGetTypeInfo(dbg->process, dbg->base, id, TI_FINDCHILDREN, tfp);
            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
            if (ret) return tfp;
            HeapFree(GetProcessHeap(), 0, tfp);
        }
    }
    return NULL;
}

struct field_info_s
{
    const WCHAR* name;
    enum SymTagEnum tag; /* of type of field */
    ULONG offset; /* in structure */
    ULONG size; /* of field, underlying integral for bitfields */
    ULONG bits_pos; /* bitfield when != ~0UL, number of bits from offset */
    ULONG bits_length; /* bitfield when != ~0UL, number of bits for field */
    ULONG array_count; /* ~0UL when not an array, otherwise number of elements in array */
};

struct foobar_info_s
{
    unsigned machine;
    unsigned size;
    struct field_info_s fields[6];
};

/* The contents here must be kept in sync with struct foobar definition */
static struct foobar_info_s foobar_info_machine[] = {
{
    IMAGE_FILE_MACHINE_AMD64,
    112,
    {
        {L"ch",    SymTagBaseType,    0,  1, ~0UL, ~0UL, ~0UL},
        {L"array", SymTagArrayType,   8, 96, ~0UL, ~0UL,   12},
        {L"bf1",   SymTagBaseType,  104,  4,    0,    3, ~0UL},
        {L"bf2",   SymTagBaseType,  104,  4,    3,    7, ~0UL},
        {L"bf3",   SymTagBaseType,  104,  4,   10,    2, ~0UL},
        {L"u",     SymTagBaseType,  108,  4, ~0UL, ~0UL, ~0UL},
    },
},
{
    IMAGE_FILE_MACHINE_I386,
    60,
    {
        {L"ch",    SymTagBaseType,    0,  1, ~0UL, ~0UL, ~0UL},
        {L"array", SymTagArrayType,   4, 48, ~0UL, ~0UL,   12},
        {L"bf1",   SymTagBaseType,   52,  4,    0,    3, ~0UL},
        {L"bf2",   SymTagBaseType,   52,  4,    3,    7, ~0UL},
        {L"bf3",   SymTagBaseType,   52,  4,   10,    2, ~0UL},
        {L"u",     SymTagBaseType,   56,  4, ~0UL, ~0UL, ~0UL},
    },
},
};
static struct foobar_info_s* foobar_info;

static BOOL set_machine_info(const struct debuggee* dbg)
{
    unsigned i;
    for (i = 0; i < ARRAYSIZE(foobar_info_machine); ++i)
        if (foobar_info_machine[i].machine == dbg->machine)
        {
            foobar_info = &foobar_info_machine[i];
            return TRUE;
        }
    fprintf(stderr, "Unsupported machine 0x%x\n", dbg->machine);
    return FALSE;
}

static struct
{
    const WCHAR* name;
    int value;
}
myenum_values[] =
{
    {L"FIRST_VALUE",   0},
    {L"SECOND_VALUE",  1},
    {L"THIRD_VALUE",  57},
    {L"FOURTH_VALUE", 58},
};

static void test_symname(const struct debuggee* dbg)
{
    char sibuf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* siA = (SYMBOL_INFO*)sibuf;
    SYMBOL_INFOW* siW = (SYMBOL_INFOW*)sibuf;
    BOOL ret;

    siA->SizeOfStruct = sizeof(*siA);
    siA->MaxNameLen = sizeof(sibuf) - sizeof(*siA);

    ret = SymFromName(dbg->process, "function_AA", siA);
    ok(ret, "Couldn't get function_AA: %u\n", GetLastError());
    ok(siA->NameLen == 11, "Wrong length %u\n", siA->NameLen);
    ok(!strcmp(siA->Name, "function_AA"), "Wrong name %s %u\n", siA->Name);

    siW->SizeOfStruct = sizeof(*siW);
    siW->MaxNameLen = 8;

    ret = SymFromNameW(dbg->process, L"function_AA", siW);
    ok(ret, "Couldn't get function_AA: %u\n", GetLastError());
    ok(siW->NameLen == 11, "Wrong length %u\n", siW->NameLen);
    ok(!lstrcmpW(siW->Name, L"functio"), "Wrong name %ls\n", siW->Name);
}

static void test_symbols(const struct debuggee* dbg)
{
    /* we inspect (with dbghelp) all those local variables & function parameters
     * so check out the code below when changing those variables
     */
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    DWORD type, tag, bt, kind;
    ULONG64 len;
    WCHAR* name;
    TI_FINDCHILDREN_PARAMS* tfp;

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = SymFromName(dbg->process, "function_AA", si);
    ok(ret, "SymFromName failed: %u\n", GetLastError());
    ok(si->NameLen == 11, "Wrong name len %u\n", si->NameLen);
    ok(!strcmp(si->Name, "function_AA"), "Wrong name %s\n", si->Name);
    ok(si->Size, "Should have a size\n");
    ok(si->ModBase == dbg->base, "Wrong module base: expecting %s but got %s\n",
       wine_dbgstr_longlong(dbg->base), wine_dbgstr_longlong(si->ModBase));
    /* FIXME on W10, we get 0 in Flags... */
    ok(si->Tag == SymTagFunction, "Wrong tag %u\n", si->Tag);

    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_TYPEID, &type);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(type == si->TypeIndex, "wrong typeid %u (expecting %u)\n", type, si->TypeIndex);

    /* checking return type in signature */
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_TYPEID, &type);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(tag == SymTagBaseType, "wrong tag %u\n", tag);
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
    todo_wine
    ok(!ret, "SymGetTypeInfo should fail\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_BASETYPE, &bt);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(bt == btUInt, "Wrong base-type %u\n", bt);

    /* checking return parameters's type in signature */
    tfp = retrieve_children(dbg, si->TypeIndex);
    if (tfp)
    {
        ok(tfp->Count == 2, "Wrong number of parameters' type\n");

        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[0], TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagFunctionArgType, "wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[0], TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagPointerType, "Wrong base-type %u\n", bt);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagUDT, "wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        if (ret)
        {
            ok(!lstrcmpW(name, L"foobar"), "UDT name is %ls\n", name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[1], TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagFunctionArgType, "wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[1], TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        type = untypedef(dbg, type);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        todo_wine
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_BASETYPE, &bt);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(bt == btUInt, "Wrong base-type %u\n", bt);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(len == sizeof(DWORD64), "Wrong length %s (expecting %u)\n",
           wine_dbgstr_longlong(len), sizeof(DWORD64));

        HeapFree(GetProcessHeap(), 0, tfp);
    }

    /* Checking children (params, local variables) */
    tfp = retrieve_children(dbg, si->Index);
    if (tfp)
    {
        DWORD i, j;
        static struct
        {
            const WCHAR* name;
            SIZE_T size;
            enum DataKind kind;
            enum DataKind other_kind; /* appears with CL.EXE /O2 (but not with CL.EXE; maybe a bug?) */
        } locpmts[] =
        {
            /* we assume we have same size on this exec and children process */
            {L"base",    sizeof(DWORD64),  DataIsParam, DataIsLocal},
            {L"other",   sizeof(DWORD),    DataIsLocal, DataIsLocal},
        };
        BOOL labelfound = FALSE;

        /* 4 made of: 2 parameters, one local, one label */
        ok(tfp->Count >= 4, "Wrong number of parameters+local variables' type %u\n", tfp->Count);

        /* there are other children than parameters / local variables, so need to lookup */
        for (j = 0; j < ARRAYSIZE(locpmts); ++j)
        {
            BOOL found = FALSE;
            winetest_push_context("%ls (#%u)", locpmts[j].name, j);
            for (i = 0; i < tfp->Count; ++i)
            {
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                switch (tag)
                {
                case SymTagData: /* the parameters and local variables */
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                    if (ret)
                    {
                        if (!lstrcmpW(name, locpmts[j].name))
                        {
                            found = TRUE;
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_TYPE, &type);
                            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                            ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
                            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                            ok(len == locpmts[j].size, "Local variable/parameter %ls's size is wrong %s (expecting %s)\n",
                               name, wine_dbgstr_longlong(len), wine_dbgstr_longlong(locpmts[j].size));
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_OFFSET, &len);
                            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_DATAKIND, &kind);
                            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                            ok((enum DataKind)kind == locpmts[j].kind || (enum DataKind)kind == locpmts[j].other_kind,
                               "Local variable/parameter %ls's kind is wrong %u (expecting %u)\n", name, kind, locpmts[j].kind);
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_LEXICALPARENT, &type);
                            ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                            ok(type == si->Index, "wrong lexical parent %u\n", type);
                        }
                        HeapFree(GetProcessHeap(), 0, name);
                    }
                    break;
                case SymTagLabel: /* labels inside the function */
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                    if (ret)
                    {
                        labelfound |= endswithW(name, L"label"); /* CL.EXE prepends a '$' to the name of the label */
                        HeapFree(GetProcessHeap(), 0, name);
                    }
                    break;
                default:
                    break;
                }
            }
            winetest_pop_context();
            ok(found, "Couldn't find local variable/parameter %ls\n", locpmts[j].name);
        }
        ok(labelfound, "Couldn't find the label 'label'\n");
        HeapFree(GetProcessHeap(), 0, tfp);
    }
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_ADDRESS, &len);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
       "Wrong address %s wrt [%s, %s]\n", wine_dbgstr_longlong(si->Address), wine_dbgstr_longlong(dbg->base),
       wine_dbgstr_longlong(dbg->base + dbg->size));
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &type);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(tag == SymTagCompiland, "Wrong tag %u\n", tag);
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    if (ret)
    {
        ok(isfile("debuggee", ".o", name) ||
           isfile("debuggee", ".obj", name) ||
           (ok_running_wine && isfile("debuggee", ".c", name)), // FIXME better way?
           "Wrong compiland name %ls\n", name);
        HeapFree(GetProcessHeap(), 0, name);
    }

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = SymFromName(dbg->process, "myfoobar", si);
    ok(ret, "SymFromName failed: %u\n", GetLastError());
    ok(si->NameLen == 8, "Wrong name len %u\n", si->NameLen);
    ok(!strcmp(si->Name, "myfoobar"), "Wrong name %s\n", si->Name);
    ok(si->Size == foobar_info->size, "Wrong size %u\n", si->Size);
    ok(si->ModBase == dbg->base, "Wrong module base: expecting %s but got %s\n",
       wine_dbgstr_longlong(dbg->base), wine_dbgstr_longlong(si->ModBase));
    /* FIXME on W10, we get 0 in Flags... */
    ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
       "Wrong address %s wrt [%s, %s]\n", wine_dbgstr_longlong(si->Address), wine_dbgstr_longlong(dbg->base),
       wine_dbgstr_longlong(dbg->base + dbg->size));
    ok(si->Tag == SymTagData, "Wrong tag %u\n", si->Tag);

    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_ADDRESS, &len);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
       "Wrong address %s wrt [%s, %s]\n", wine_dbgstr_longlong(si->Address), wine_dbgstr_longlong(dbg->base),
       wine_dbgstr_longlong(dbg->base + dbg->size));
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_TYPE, &type);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(type == si->TypeIndex, "Wrong type\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &len);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(len == foobar_info->size, "Wrong size %s\n", wine_dbgstr_longlong(len));
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &type);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    ok(tag == SymTagCompiland, "Wrong tag %u\n", tag);
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
    if (ret)
    {
        ok(isfile("debuggee", ".o", name) ||
           isfile("debuggee", ".obj", name) ||
           (ok_running_wine && isfile("debuggee", ".c", name)), // FIXME better way?
           "Wrong compiland name %ls\n", name);
        HeapFree(GetProcessHeap(), 0, name);
    }
}

static void test_types(const struct debuggee* dbg)
{
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    TI_FINDCHILDREN_PARAMS* tfp;

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = SymGetTypeFromName(dbg->process, dbg->base, "foobar", si);
    ok(ret, "SymGetTypeFromName failed: %u\n", GetLastError());
    if (ret)
    {
        DWORD tag, info, type;
        ULONG64 len;
        WCHAR* name;
        unsigned i;

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_UDTKIND, &info);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(info == UdtStruct, "Unexpected udt kind %u\n", info);

        ok(si->NameLen == 6, "Wrong name len but got %u\n", si->NameLen);
        ok(!strcmp(si->Name, "foobar"), "Wrong name, expecting foobar but got %s\n", debugstr_a(si->Name));
        ok(si->Size == foobar_info->size, "Wrong size, got %u\n", si->Size);
        ok(si->Index == si->TypeIndex, "Index and TypeIndex shoud be identical\n");
        ok(si->ModBase == dbg->base, "Wrong module base\n");
        ok(si->Tag == SymTagUDT, "Unexpected tag %u\n", si->Tag);

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagUDT, "Unexpected tag 0x%x\n", tag);

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_CLASSPARENTID, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LEXICALPARENT, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagExe, "Wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        if (ret)
        {
            ok(ismodulename(dbg->dll_name, name), "Unexpected name %ls %s\n", name, dbg->dll_name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        tfp = retrieve_children(dbg, si->TypeIndex);
        if (tfp)
        {
            ok(tfp->Count == ARRAYSIZE(foobar_info->fields), "Wrong number of fields in foobar %u\n", tfp->Count);

            for (i = 0; i < ARRAYSIZE(foobar_info->fields); ++i)
            {
                winetest_push_context("%ls (%u)", foobar_info->fields[i].name, i);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                if (ret)
                {
                    ok(!lstrcmpW(name, foobar_info->fields[i].name), "Wrong field name got %ls (expecting %s)\n",
                       name, foobar_info->fields[i].name);
                    ok(HeapSize(GetProcessHeap(), 0, name) != (SIZE_T)-1,
                       "Expecting name to be allocated on process heap\n");
                    HeapFree(GetProcessHeap(), 0, name);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_TYPE, &type);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                ok((enum SymTagEnum)tag == foobar_info->fields[i].tag, "Wrong tag %u (expecting %u)\n",
                   tag, foobar_info->fields[i].tag);
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                ok(len == foobar_info->fields[i].size, "Wrong length %s (expecting %u)\n",
                   wine_dbgstr_longlong(len), foobar_info->fields[i].size);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_OFFSET, &info);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                ok(info == foobar_info->fields[i].offset, "Wrong offset %u (expecting %u)\n",
                   info, foobar_info->fields[i].offset);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_BITPOSITION, &info);
                if (foobar_info->fields[i].bits_length == ~0UL)
                    ok(!ret, "SymGetTypeInfo shouldn't succeed\n");
                else
                {
                    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                    ok(info == foobar_info->fields[i].bits_pos, "Wrong bit position %u (expecting %u)\n",
                       info, foobar_info->fields[i].bits_pos);
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_LENGTH, &len);
                    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                    ok(len == foobar_info->fields[i].bits_length, "Wrong bit length %s (expecting %u)\n",
                       wine_dbgstr_longlong(len), foobar_info->fields[i].bits_length);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_COUNT, &info);
                if (foobar_info->fields[i].array_count == ~0UL)
                    ok(!ret, "SymGetTypeInfo shouldn't succeed\n");
                else
                {
                    ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                    ok(info == foobar_info->fields[i].array_count,
                       "Wrong array count position %u (expecting %u)\n",
                       info, foobar_info->fields[i].array_count);
                }
                winetest_pop_context();
            }
            HeapFree(GetProcessHeap(), 0, tfp);
        }
    }

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = SymGetTypeFromName(dbg->process, dbg->base, "myenum", si);
    ok(ret, "SymGetTypeFromName failed: %u\n", GetLastError());
    if (ret)
    {
        DWORD tag, info, type;
        DWORD64 len, len2;
        WCHAR* name;
        unsigned i;
        VARIANT v;

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_UDTKIND, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");

        ok(si->NameLen == 6, "Wrong name len but got %u\n", si->NameLen);
        ok(!strcmp(si->Name, "myenum"), "Wrong name, expecting foobar but got %s\n", debugstr_a(si->Name));
        ok(si->Index == si->TypeIndex, "Index and TypeIndex shoud be identical\n");
        ok(si->ModBase == dbg->base, "Wrong module base\n");
        ok(si->Tag == SymTagEnum, "Unexpected tag %u\n", si->Tag);

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagEnum, "Unexpected tag 0x%x\n", tag);

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_CLASSPARENTID, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LEXICALPARENT, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagExe, "Wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        if (ret)
        {
            ok(ismodulename(dbg->dll_name, name), "Unexpected name %ls\n", name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(tag == SymTagBaseType, "Unexpected tag 0x%x\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(len == 1 || len == 2 || len == 4, "Unexpected length %s\n", wine_dbgstr_longlong(len));

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_BASETYPE, &info);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(info == btInt, "Wrong tag %u\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &len2);
        ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
        ok(len2 == len, "Length should match underlying type %s %s\n", wine_dbgstr_longlong(len), wine_dbgstr_longlong(len2));

        // FIXME should dig a bit more about actual integral type
        tfp = retrieve_children(dbg, si->TypeIndex);
        if (tfp)
        {
            ok(tfp->Count == ARRAYSIZE(myenum_values), "Wrong number of values in myenum %u\n", tfp->Count);

            for (i = 0; i < ARRAYSIZE(myenum_values); ++i)
            {
                winetest_push_context("%ls (#%u)", myenum_values[i].name, i);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                if (ret)
                {
                    ok(!lstrcmpW(name, myenum_values[i].name), "Wrong value name got %ls (expecting %s)\n", name, myenum_values[i].name);
                    ok(HeapSize(GetProcessHeap(), 0, name) != (SIZE_T)-1, "Expecting name to be allocated on process heap\n");
                    HeapFree(GetProcessHeap(), 0, name);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_VALUE, &v);
                ok(ret, "SymGetTypeInfo failed: %u\n", GetLastError());
                /* we have little control on the size of the underlying integral type for the enum
                 * so support various integral sizes
                 */
                switch (V_VT(&v))
                {
                case VT_I1: ret = V_I1(&v) == myenum_values[i].value; break;
                case VT_I2: ret = V_I2(&v) == myenum_values[i].value; break;
                case VT_I4: ret = V_I4(&v) == myenum_values[i].value; break;
                default: ok(FALSE, "unexpected variant type %u\n", V_VT(&v)); break;
                }
                winetest_pop_context();
            }
            HeapFree(GetProcessHeap(), 0, tfp);
        }
    }
}

void do_tests(const struct debuggee* dbg)
{
    if (!set_machine_info(dbg)) return;
    winetest_init();
    winetest_push_context(dbg->external_process ? "external" : "internal");
    test_symname(dbg);
    test_symbols(dbg);
    test_types(dbg);
    winetest_pop_context();
    winetest_report();
}
