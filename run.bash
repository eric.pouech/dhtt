#!/usr/bin/bash

# must be called with:
# run.bash <system> <dhtt.exe> <subpcs> -- <DLL>*
# but <system> could be empty
# <subpcs> can either be subprocess name, or -- when internal
red=`tput setaf 1`
green=`tput setaf 10`
lblue=`tput setaf 14`
reset=`tput sgr0`

if [ $# -ge 5 ] && [ "$4" == "--" ]; then
    WINE=$1
    shift
    TESTER=$1
    shift
    if [ "$1" == "--" ]; then
	SUBPCS=
	INEXT="internal"
    else
	SUBPCS="--subprocess ${1}"
	INEXT=$1
    fi
    shift
    #--
    shift

    while (($#)); do
	echo -e "[${green}Testing${reset}] ($WINE, $TESTER, $INEXT, ${lblue}$1${reset})"
	$WINE/wine $TESTER $SUBPCS --dll $1
	shift
    done
fi
