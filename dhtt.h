/*
 * This file is part of DHTT (DbgHelp Test Tool)
 *
 * Internals definitions.
 *
 * Copyright (C) 2021 Eric Pouech
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

struct debuggee
{
    const char* dll_name;
    BOOL external_process;
    HANDLE process;
    HANDLE thread;
    USHORT machine;
    DWORD64 base;
    DWORD64 size;
};

extern void do_tests(const struct debuggee* dbg);
